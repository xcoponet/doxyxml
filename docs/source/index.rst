.. DoxyXML documentation master file, created by
   sphinx-quickstart on Wed Jun 17 14:23:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DoxyXML's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Code Documentation
==================

.. automodule:: DoxyXML.index_parser
   :members:

.. automodule:: DoxyXML.class_obj
   :members:

.. automodule:: DoxyXML.class_member
   :members:

