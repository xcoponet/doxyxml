import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="DoxyXML",
    version="0.1.0",
    author="Xavier Coponet",
    author_email="xavier.coponet@gmail.com",
    url="https://gitlab.com/xcoponet/doxyxml",
    description="Parse XML documentation generated by Doxygen",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license='MIT',
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        ],
    project_urls={
        'Documentation': 'https://xcoponet.gitlab.io/doxyxml/',
    },
)
