/**
 * @file    inc\rectangle.h.
 *
 * @brief   Declares the rectangle class.
 */
 
#pragma once

#include "shape.h"
#include "MyFloat.h"
#include <math.h>

namespace Geometry
{
/**
 * @class   Rectangle
 *
 * @brief   A rectangle.
 *
 * Inherits the Shape class
 */

class Rectangle : public Shape
{
public:

    /**
     * @fn  Rectangle::Rectangle();
     *
     * @brief   Default constructor.
     */

    Rectangle();

    /**
     * @fn  Rectangle::Rectangle( float w, float h );
     *
     * @brief   Constructor.
     *
     * @param   w   The width.
     * @param   h   The height.
     */

    Rectangle( float w, float h );

    /*!
     * @brief destructor
     */
    virtual ~Rectangle(){};

    /**
     * @brief   Gets the area.
     *
     * @return  the computed area
     */

    float area()const;

#pragma region Getter_Setter

    /**
     * @brief   width setter.
     *
     * @param   w   The width.
     */
    inline void setWidth( const float w );

    /**
     * @brief   height setter.
     *
     * @param   h   The height.
     */
    inline void setHeight( const float h );

    /**
     * @brief   Gets the height.
     *
     * @return  A float.
     */
    inline float height()const;

    /**
     * @brief   Gets the width.
     *
     * @return  A float.
     */
    inline float width()const;
#pragma endregion


private:  
    MyFloat m_width;/** @brief   The width */
    float m_height;/** @brief   The height */
};

/*!
 * @brief compute the square value of a number
 * @param a the square value to compute
 * @return the squared value of a
 */
inline float square(const float a)
{
    return a*a;
}

/*!
 * @brief compute the square root of a number
 * @param a the square root value to compute
 * @return the square root value of a
 */
inline float sqrt(const float a)
{
    return sqrt(a);
}

}
