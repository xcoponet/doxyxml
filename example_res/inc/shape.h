/**
 * @file    inc\shape.h.
 *
 * @brief   Declares the shape class.
 */

 /**
  * @namespace   Geometry
  *
  * @brief   Geometric forms classes
  */

#pragma once

namespace Geometry
{



/**
 * @class   Shape
 *
 * @brief   A shape.
 *
 * @author  Xavier Coponet
 * @date    31/07/2019
 */
class Shape
{
public:

    /**
     * @fn  Shape::Shape();
     *
     * @brief   Default constructor.
     */
    Shape();

    /**
     * @brief   Gets the shape area.
     *
     * @return  A the computed area
     */

    virtual float area()const = 0;
};

} // namespace