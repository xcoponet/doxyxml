#include "rectangle.h"

namespace Geometry
{

Rectangle::Rectangle():m_width(0),m_height(0)
{
}

Rectangle::Rectangle( float w, float h ):m_width(w),m_height(h)
{
}

float Rectangle::area() const
{
    return m_width.m_f * m_height;
}

inline void Rectangle::setWidth( const float w )
{
    m_width.m_f = w;
}

inline void Rectangle::setHeight( const float h )
{
    m_height = h;
}

inline float Rectangle::height() const
{
    return m_height;
}

inline float Rectangle::width() const
{
    return m_width.m_f;
}

}

